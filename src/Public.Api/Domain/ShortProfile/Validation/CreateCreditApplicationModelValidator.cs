﻿using System;
using FluentValidation;
using Public.Api.Domain.Validation;
using CreateCreditApplicationModel = Public.Api.Models.ShortProfile.CreateCreditApplicationModel;

namespace Public.Api.Domain.ShortProfile.Validation
{
    public class CreateCreditApplicationModelValidator: AbstractValidator<CreateCreditApplicationModel>
    {
        public CreateCreditApplicationModelValidator()
        {
            RuleFor(x => x.LastName)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(ValidationConstants.CYRILLIC_REGEX).WithMessage(ValidationMessages.ONLY_CYRILLIC_SYMBOLS_AND_HYPHEN);
            
            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(ValidationConstants.CYRILLIC_REGEX).WithMessage(ValidationMessages.ONLY_CYRILLIC_SYMBOLS_AND_HYPHEN);
            
            RuleFor(x => x.MiddleName)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(ValidationConstants.CYRILLIC_REGEX).WithMessage(ValidationMessages.ONLY_CYRILLIC_SYMBOLS_AND_HYPHEN);
            
            RuleFor(x => x.DateOfBirth)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Must(x => x.Date < DateTime.Now.AddYears(-18)).WithMessage(ValidationMessages.AGE_TOO_SMALL)
                .Must(x => x.Date > DateTime.Now.AddYears(-120)).WithMessage(ValidationMessages.AGE_TOO_BIG);

            RuleFor(x => x.PhoneNumber)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(ValidationConstants.PHONE_NUMBER_REGEX).WithMessage(ValidationMessages.PHONE_IS_INVALID);
            
            RuleFor(x => x.AdditionalPhoneNumber)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(ValidationConstants.PHONE_NUMBER_REGEX).WithMessage(ValidationMessages.PHONE_IS_INVALID);
            
            RuleFor(x => x.ResidenceAddressRegion)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);
            
            RuleFor(x => x.ResidenceAddressRegionKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);

            RuleFor(x => x.ClientConsentReceived)
                .NotEmpty().WithMessage(ValidationMessages.CLIENT_CONSENT_RECEIVED_INVALID);
        }
    }
}