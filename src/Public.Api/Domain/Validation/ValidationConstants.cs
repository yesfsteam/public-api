﻿﻿namespace Public.Api.Domain.Validation
{
    public class ValidationConstants
    {
        public const string PHONE_NUMBER_REGEX = "^7[0-9]{10}$";
        public const string CYRILLIC_REGEX = @"^[\p{IsCyrillic}\-]+$";
    }
}