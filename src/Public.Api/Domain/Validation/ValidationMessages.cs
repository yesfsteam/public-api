﻿﻿namespace Public.Api.Domain.Validation
{
    public class ValidationMessages
    {
        public const string INVALID_VALUE = "Недопустимое значение";
        public const string EMPTY_VALUE = "Поле должно быть заполнено";
        public const string NOT_EMPTY_VALUE = "Поле не должно быть заполнено";
        public const string ONLY_CYRILLIC_SYMBOLS_AND_HYPHEN = "Поле может содержать только русские буквы и знак дефис";
        public const string EMAIL_IS_INVALID = "Некорректный e-mail адрес";
        public const string PHONE_IS_INVALID = "Некорректный номер телефона";
        public const string FUTURE_DATE_NOT_ALLOWED = "Дата не может быть больше текущей";
        public const string NUMERIC_MORE_THAN_ZERO = "Значение должно быть больше нуля";
        
        public const string CLIENT_CONSENT_RECEIVED_INVALID = "Согласие на обработку данных неподтверждено";
        
        public const string CONFIRMATION_CODE_INVALID = "Код подтверждения должен содержать 4 цифры";
        
        public const string AGE_TOO_BIG = "Возраст заемщика не может быть более 120 лет";
        public const string AGE_TOO_SMALL = "Возраст заемщика не может быть менее 18 лет";
        public const string PASSPORT_SERIES_INVALID = "Серия паспорта должена содержать 4 цифры";
        public const string PASSPORT_NUMBER_INVALID = "Номер паспорта должен содержать 6 цифр";
        public const string PASSPORT_ISSUE_DATE_INVALID = "Некорректное значение даты выдачи паспорта";
        public const string PASSPORT_DEPARTMENT_CODE_INVALID = "Код подразделения должен быть в формате 999-999";
        public const string DEPENDENTS_COUNT_TOO_BIG = "Количество детей не может быть больше 50";
        
        public const string EMPLOYER_TIN_INVALID = "ИНН должен содержать 10 или 12 цифр";
    }
}