﻿using FluentValidation;
using Yes.CreditApplication.Api.Contracts.Public;

namespace Public.Api.Domain.Validation
{
    public class CreditApplicationConfirmationCodeModelValidator: AbstractValidator<CreditApplicationConfirmationCodeModel>
    {
        public CreditApplicationConfirmationCodeModelValidator()
        {
            RuleFor(x => x.ConfirmationCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(@"^\d{4}$").WithMessage(ValidationMessages.CONFIRMATION_CODE_INVALID);
        }
    }
}