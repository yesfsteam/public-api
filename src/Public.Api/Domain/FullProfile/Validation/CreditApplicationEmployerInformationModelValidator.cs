﻿using System;
using FluentValidation;
using Public.Api.Domain.Validation;
using Yes.CreditApplication.Api.Contracts.Enums;
using Yes.CreditApplication.Api.Contracts.Public.FullProfile;

namespace Public.Api.Domain.FullProfile.Validation
{
    public class CreditApplicationEmployerInformationModelValidator: AbstractValidator<CreditApplicationEmployerInformationModel>
    {
        public CreditApplicationEmployerInformationModelValidator()
        {
            RuleFor(x => x.Activity)
                .IsInEnum().WithMessage(ValidationMessages.INVALID_VALUE);
            
            RuleFor(x => x.Tin)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(@"^(\d{10}|\d{12})$").WithMessage(ValidationMessages.EMPLOYER_TIN_INVALID);

            RuleFor(x => x.MonthlyIncome)
                .GreaterThan(0).WithMessage(ValidationMessages.NUMERIC_MORE_THAN_ZERO);
            
            RuleFor(x => x.EmployerName)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerName)
                .Must(x => x == null).WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);
            
            RuleFor(x => x.EmployerAddressRegion)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerAddressRegion)
                .Must(x => x == null).WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);

            RuleFor(x => x.EmployerAddressRegionKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerAddressRegionKladrCode)
                .Must(x => x == null).WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);
            
            RuleFor(x => x.EmployerAddressCity)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerAddressCity)
                .Must(x => x == null).WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);

            RuleFor(x => x.EmployerAddressCityKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerAddressCityKladrCode)
                .Must(x => x == null).WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);
            
            RuleFor(x => x.EmployerAddressStreet)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerAddressStreet)
                .Must(x => x == null).WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);
            
            RuleFor(x => x.EmployerAddressStreetKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerAddressStreetKladrCode)
                .Must(x => x == null).WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);
            
            RuleFor(x => x.EmployerAddressHouse)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerAddressHouse)
                .Must(x => x == null).WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);
            
            RuleFor(x => x.EmployerAddressApartment)
                .Must(x => x == null).WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);

            RuleFor(x => x.EmployerAddressKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerAddressKladrCode)
                .Must(x => x == null).WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);

            RuleFor(x => x.EmployerIndustry)
                .Must(x => x.HasValue).WithMessage(ValidationMessages.EMPTY_VALUE)
                .IsInEnum().WithMessage(ValidationMessages.INVALID_VALUE)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerIndustry)
                .Empty().WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);

            RuleFor(x => x.EmployerStaff)
                .Must(x => x.HasValue).WithMessage(ValidationMessages.EMPTY_VALUE)
                .GreaterThan(0).WithMessage(ValidationMessages.NUMERIC_MORE_THAN_ZERO)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerStaff)
                .Empty().WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);

            RuleFor(x => x.EmployerPhoneNumber)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(ValidationConstants.PHONE_NUMBER_REGEX).WithMessage(ValidationMessages.PHONE_IS_INVALID)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployerPhoneNumber)
                .Must(x => x == null).WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);
            
            RuleFor(x => x.EmployeeStartDate)
                .Must(x => x.HasValue).WithMessage(ValidationMessages.EMPTY_VALUE)
                .Must(x => x.HasValue && x.Value.Date < DateTime.Now).WithMessage(ValidationMessages.FUTURE_DATE_NOT_ALLOWED)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployeeStartDate)
                .Empty().WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);

            RuleFor(x => x.EmployeePosition)
                .Must(x => x.HasValue).WithMessage(ValidationMessages.EMPTY_VALUE)
                .IsInEnum().WithMessage(ValidationMessages.INVALID_VALUE)
                .When(x => x.Activity == Activity.Employee);
            RuleFor(x => x.EmployeePosition)
                .Empty().WithMessage(ValidationMessages.NOT_EMPTY_VALUE)
                .When(x => x.Activity != Activity.Employee);
        }
    }
}