﻿using FluentValidation;
using Public.Api.Domain.Validation;
using CreateCreditApplicationModel = Public.Api.Models.FullProfile.CreateCreditApplicationModel;

namespace Public.Api.Domain.FullProfile.Validation
{
    public class CreateCreditApplicationModelValidator: AbstractValidator<CreateCreditApplicationModel>
    {
        private const string CREDIT_AMOUNT_TOO_SMALL = "Минимальная сумма кредита 3 000 ₽";
        private const string CREDIT_AMOUNT_TOO_BIG = "Максимальная сумма кредита 1 000 000 ₽";
        private const string CREDIT_PERIOD_TOO_SMALL = "Минимальный срок кредита 1 месяц";
        private const string CREDIT_PERIOD_TOO_BIG = "Максимальный срок кредита 7 лет";
        
        public CreateCreditApplicationModelValidator()
        {
            RuleFor(x => x.CreditAmount)
                .GreaterThanOrEqualTo(3000).WithMessage(CREDIT_AMOUNT_TOO_SMALL)
                .LessThanOrEqualTo(1000000).WithMessage(CREDIT_AMOUNT_TOO_BIG);

            RuleFor(x => x.CreditPeriod)
                .GreaterThanOrEqualTo(1).WithMessage(CREDIT_PERIOD_TOO_SMALL)
                .LessThanOrEqualTo(84).WithMessage(CREDIT_PERIOD_TOO_BIG);
            
            RuleFor(x => x.LastName)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(ValidationConstants.CYRILLIC_REGEX).WithMessage(ValidationMessages.ONLY_CYRILLIC_SYMBOLS_AND_HYPHEN);
            
            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(ValidationConstants.CYRILLIC_REGEX).WithMessage(ValidationMessages.ONLY_CYRILLIC_SYMBOLS_AND_HYPHEN);
            
            RuleFor(x => x.MiddleName)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(ValidationConstants.CYRILLIC_REGEX).WithMessage(ValidationMessages.ONLY_CYRILLIC_SYMBOLS_AND_HYPHEN);
            
            RuleFor(x => x.Email)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .EmailAddress().WithMessage(ValidationMessages.EMAIL_IS_INVALID);
            
            RuleFor(x => x.PhoneNumber)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(ValidationConstants.PHONE_NUMBER_REGEX).WithMessage(ValidationMessages.PHONE_IS_INVALID);
            
            RuleFor(x => x.ClientConsentReceived)
                .NotEmpty().WithMessage(ValidationMessages.CLIENT_CONSENT_RECEIVED_INVALID);
        }
    }
}