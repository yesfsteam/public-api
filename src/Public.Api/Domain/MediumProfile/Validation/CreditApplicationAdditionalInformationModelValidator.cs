﻿using System;
using FluentValidation;
using Public.Api.Domain.Validation;
using Yes.CreditApplication.Api.Contracts.Public.MediumProfile;

namespace Public.Api.Domain.MediumProfile.Validation
{
    public class CreditApplicationAdditionalInformationModelValidator: AbstractValidator<CreditApplicationAdditionalInformationModel>
    {
        public CreditApplicationAdditionalInformationModelValidator()
        {
            RuleFor(x => x.DateOfBirth)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Must(x => x.Date < DateTime.Now.AddYears(-18)).WithMessage(ValidationMessages.AGE_TOO_SMALL)
                .Must(x => x.Date > DateTime.Now.AddYears(-120)).WithMessage(ValidationMessages.AGE_TOO_BIG);
            
            RuleFor(x => x.PlaceOfBirth)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);
            
            RuleFor(x => x.Gender)
                .IsInEnum().WithMessage(ValidationMessages.INVALID_VALUE);
            
            RuleFor(x => x.MonthlyIncome)
                .GreaterThan(0).WithMessage(ValidationMessages.NUMERIC_MORE_THAN_ZERO);
            
            RuleFor(x => x.PassportSeries)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(@"^\d{4}$").WithMessage(ValidationMessages.PASSPORT_SERIES_INVALID);
            
            RuleFor(x => x.PassportNumber)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(@"^\d{6}$").WithMessage(ValidationMessages.PASSPORT_NUMBER_INVALID);
            
            RuleFor(x => x.PassportIssuer)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);
            
            RuleFor(x => x.PassportIssueDate)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Must(x => x.Date < DateTime.Now).WithMessage(ValidationMessages.FUTURE_DATE_NOT_ALLOWED)
                .Must(x => x.Date > DateTime.Now.AddYears(-120)).WithMessage(ValidationMessages.PASSPORT_ISSUE_DATE_INVALID);

            RuleFor(x => x.PassportDepartmentCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .Matches(@"^\d{3}-\d{3}$").WithMessage(ValidationMessages.PASSPORT_DEPARTMENT_CODE_INVALID);

            RuleFor(x => x.RegistrationAddressRegion)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);
            
            RuleFor(x => x.RegistrationAddressRegionKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);
            
            RuleFor(x => x.RegistrationAddressCity)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);
            
            RuleFor(x => x.RegistrationAddressCityKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);
            
            RuleFor(x => x.RegistrationAddressStreet)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);
            
            RuleFor(x => x.RegistrationAddressStreetKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);
            
            RuleFor(x => x.RegistrationAddressHouse)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);
            
            RuleFor(x => x.RegistrationAddressKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE);
            
            RuleFor(x => x.ResidenceAddressRegionKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => !string.IsNullOrWhiteSpace(x.ResidenceAddressRegion));
            
            RuleFor(x => x.ResidenceAddressCityKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => !string.IsNullOrWhiteSpace(x.ResidenceAddressCity));
            
            RuleFor(x => x.ResidenceAddressStreetKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x => !string.IsNullOrWhiteSpace(x.ResidenceAddressStreet));

            RuleFor(x => x.ResidenceAddressKladrCode)
                .NotEmpty().WithMessage(ValidationMessages.EMPTY_VALUE)
                .When(x =>
                    !string.IsNullOrWhiteSpace(x.ResidenceAddressRegion) &&
                    !string.IsNullOrWhiteSpace(x.ResidenceAddressCity) &&
                    !string.IsNullOrWhiteSpace(x.ResidenceAddressStreet) &&
                    !string.IsNullOrWhiteSpace(x.ResidenceAddressHouse));
        }
    }
}