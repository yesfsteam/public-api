﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Public.Api.Models.MediumProfile;
using Yes.CreditApplication.Api.Client.Public.MediumProfile;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Http;

namespace Public.Api.Domain.MediumProfile
{
    public interface ICreditApplicationManager
    {
        Task<Response<Guid>> CreateCreditApplication(CreateCreditApplicationModel model);
    }

    public class CreditApplicationManager : ICreditApplicationManager
    {
        private readonly ILogger<CreditApplicationManager> logger;
        private readonly IPublicCreditApplicationClient client;
        private readonly IHttpContextAccessor httpContextAccessor;

        public CreditApplicationManager(ILogger<CreditApplicationManager> logger, IPublicCreditApplicationClient client,
            IHttpContextAccessor httpContextAccessor)
        {
            this.logger = logger;
            this.client = client;
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task<Response<Guid>> CreateCreditApplication(CreateCreditApplicationModel model)
        {
            var beginTime = DateTime.Now;
            try
            {
                var request = new Yes.CreditApplication.Api.Contracts.Public.MediumProfile.CreateCreditApplicationModel
                {
                    CreditAmount = model.CreditAmount,
                    CreditPeriod = model.CreditPeriod,
                    LastName = model.LastName,
                    FirstName = model.FirstName,
                    MiddleName = model.MiddleName,
                    PhoneNumber = model.PhoneNumber,
                    ClientConsentReceived = model.ClientConsentReceived,
                    PartnerId = model.PartnerId,
                    RequestIp = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString()
                };
                return await client.CreateCreditApplication(request);
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Error while creating credit application. Duration: {beginTime.GetDuration()} Request: {model}");
                return Response<Guid>.InternalServerError();
            }
        }
    }
}