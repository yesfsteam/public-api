using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using Public.Api.Domain;
using Public.Api.Domain.Validation;
using Serilog;
using Yes.CreditApplication.Api.Contracts.Public;
using Yes.Infrastructure.Http.Extensions;

namespace Public.Api
{
    public class Startup
    {
	    private readonly string applicationName;
	    
	    public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
            applicationName = Assembly.GetExecutingAssembly().GetName().Name;
        }

        private readonly IConfiguration configuration;
        
        public void ConfigureServices(IServiceCollection services)
        {
	        services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddRouting(options => options.LowercaseUrls = true);
            //services.AddHttpContextAccessor();
            
            services.AddTransient<Domain.FullProfile.ICreditApplicationManager, Domain.FullProfile.CreditApplicationManager>();
            services.AddTransient<Domain.MediumProfile.ICreditApplicationManager, Domain.MediumProfile.CreditApplicationManager>();
            services.AddTransient<Domain.ShortProfile.ICreditApplicationManager, Domain.ShortProfile.CreditApplicationManager>();
            
            services.AddTransient<IValidator<CreditApplicationConfirmationCodeModel>, CreditApplicationConfirmationCodeModelValidator>();
            
	        //services.AddTransient<IValidator<Models.FullProfile.CreateCreditApplicationModel>, Domain.FullProfile.Validation.CreateCreditApplicationModelValidator>();
            //services.AddTransient<IValidator<Yes.CreditApplication.Api.Contracts.Public.FullProfile.CreditApplicationAdditionalInformationModel>, Domain.FullProfile.Validation.CreditApplicationAdditionalInformationModelValidator>();
            //services.AddTransient<IValidator<Yes.CreditApplication.Api.Contracts.Public.FullProfile.CreditApplicationEmployerInformationModel>, Domain.FullProfile.Validation.CreditApplicationEmployerInformationModelValidator>();
            
            //services.AddTransient<IValidator<Models.MediumProfile.CreateCreditApplicationModel>, Domain.MediumProfile.Validation.CreateCreditApplicationModelValidator>();
            //services.AddTransient<IValidator<Yes.CreditApplication.Api.Contracts.Public.MediumProfile.CreditApplicationAdditionalInformationModel>, Domain.MediumProfile.Validation.CreditApplicationAdditionalInformationModelValidator>();
            
            //services.AddTransient<IValidator<Models.ShortProfile.CreateCreditApplicationModel>, Domain.ShortProfile.Validation.CreateCreditApplicationModelValidator>();
            
            services.AddControllers().AddFluentValidation(options =>
            {
	            ValidatorOptions.PropertyNameResolver = CamelCasePropertyNameResolver.ResolvePropertyName;
            });
            
            services.AddHttpClientFromConfiguration<Yes.CreditApplication.Api.Client.Public.FullProfile.IPublicCreditApplicationClient, Yes.CreditApplication.Api.Client.Public.FullProfile.PublicCreditApplicationClient>(configuration, "CreditApplicationApi", "FullProfile");
            services.AddHttpClientFromConfiguration<Yes.CreditApplication.Api.Client.Public.MediumProfile.IPublicCreditApplicationClient, Yes.CreditApplication.Api.Client.Public.MediumProfile.PublicCreditApplicationClient>(configuration, "CreditApplicationApi", "MediumProfile");
            services.AddHttpClientFromConfiguration<Yes.CreditApplication.Api.Client.Public.ShortProfile.IPublicCreditApplicationClient, Yes.CreditApplication.Api.Client.Public.ShortProfile.PublicCreditApplicationClient>(configuration, "CreditApplicationApi", "ShortProfile");
            
            if (configuration.GetValue<bool>("EnableSwagger"))
	            services.AddSwaggerGen(c =>
	            {
		            c.SwaggerDoc("v1", new OpenApiInfo { Title = applicationName, Version = "v1" });
		            c.SwaggerDoc("v2", new OpenApiInfo { Title = applicationName, Version = "v2" });
		            c.SwaggerDoc("v3", new OpenApiInfo { Title = applicationName, Version = "v3" });
		            c.IncludeXmlComments(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, $"{applicationName}.xml"));

		            c.DocInclusionPredicate((version, apiDescription) =>
		            {
			            if (apiDescription.RelativePath.Contains("/" + version + "/"))
			            {
				            return true;
			            }
			            return false;
		            });
	            });
            services.AddCors(options =>
            {
	            options.AddPolicy("DevCorsPolicy", builder =>
	            {
		            builder
			            .AllowAnyMethod()
			            .AllowAnyHeader()
			            .AllowAnyOrigin();
	            });
	            options.AddPolicy("ProdCorsPolicy", builder =>
	            {
		            builder
			            .AllowAnyMethod()
			            .AllowAnyHeader()
			            .WithOrigins(configuration["LandingUrl"]);
	            });
            });

			Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
		}

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
        {
	        applicationLifetime.ApplicationStopping.Register(OnApplicationStopping);

	        app.UseCors(env.IsProduction() ? "ProdCorsPolicy" : "DevCorsPolicy");
	        //app.UseHsts();
            //app.UseHttpsRedirection();
            app.UseRouting();
            app.UseEndpoints(endpoints => endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}"));

            if (configuration.GetValue<bool>("EnableSwagger"))
            {
	            app.UseSwagger();
	            app.UseSwaggerUI(c =>
	            {
		            c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{applicationName} v1 FullProfile");
		            c.SwaggerEndpoint("/swagger/v2/swagger.json", $"{applicationName} v2 MediumProfile");
		            c.SwaggerEndpoint("/swagger/v3/swagger.json", $"{applicationName} v3 ShortProfile");
		            c.RoutePrefix = string.Empty;
	            });
            }
            Log.Logger.Information($"{applicationName} has been started");
        }

        private void OnApplicationStopping()
        {
	        Log.Logger.Information($"{applicationName} has been stopped");
        }
	}
}