﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Public.Api.Domain.ShortProfile;
using Yes.CreditApplication.Api.Client.Public.ShortProfile;
using Yes.CreditApplication.Api.Contracts.Public;
using Yes.Infrastructure.Common.Models;
using CreateCreditApplicationModel = Public.Api.Models.ShortProfile.CreateCreditApplicationModel;

namespace Public.Api.Controllers.ShortProfile
{
    [Route("api/v3/credit-applications")]
    public class CreditApplicationsController : ControllerBase
    {
        private readonly IPublicCreditApplicationClient client;
        private readonly ICreditApplicationManager manager;

        public CreditApplicationsController(IPublicCreditApplicationClient client, ICreditApplicationManager manager)
        {
            this.client = client;
            this.manager = manager;
        }
        
        /// <summary>
        /// Создает заявку на кредит (Шаг 1)
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorModel), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateCreditApplication(CreateCreditApplicationModel model)
        {
            var response = await manager.CreateCreditApplication(model);
            return MakeResponse(response);
        }
        
        /// <summary>
        /// Отправляет новый код подтверждения (Шаг 2)
        /// </summary>
        [HttpPost("{creditApplicationId}/new-confirmation-code")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ResendConfirmationCode([FromRoute]Guid creditApplicationId)
        {
            var response = await client.ResendConfirmationCode(creditApplicationId);
            return MakeResponse(response);
        }
        
        /// <summary>
        /// Подтверждает номер телефона клиента (Шаг 2)
        /// </summary>
        [HttpPost("{creditApplicationId}/confirmation-code")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorModel), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ConfirmPhoneNumber([FromRoute]Guid creditApplicationId, CreditApplicationConfirmationCodeModel model)
        {
            var response = await client.ConfirmPhoneNumber(creditApplicationId, model);
            return MakeResponse(response);
        }
    }
}
