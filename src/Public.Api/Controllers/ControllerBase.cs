﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Public.Api.Domain.Validation;
using Serilog;
using Yes.Infrastructure.Common.Extensions;
using Yes.Infrastructure.Common.Models;
using Yes.Infrastructure.Http;

namespace Public.Api.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class ControllerBase: Controller
    {
        protected IActionResult MakeResponse<T>(Response<T> result)
        {
            if (!result.IsSuccessStatusCode)
            {
                return StatusCode((int)result.StatusCode, result.ErrorMessage);
            }

            return Ok(result.Content);
        }
        
        protected IActionResult MakeResponse<T, TBadRequestModel>(Response<T, TBadRequestModel> result)
        {
            if (!result.IsSuccessStatusCode)
            {
                return StatusCode((int)result.StatusCode, result.ErrorModel);
            }

            return Ok(result.Content);
        }
        
        protected IActionResult MakeResponse(Response result)
        {
            if (!result.IsSuccessStatusCode)
            {
                return StatusCode((int)result.StatusCode, result.ErrorMessage);
            }

            return Ok();
        }
        
        public override Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!ModelState.IsValid)
            {
                var result = new ErrorModel { Errors = new Dictionary<string, string>() };

                foreach (var value in context.ModelState.Values)
                {
                    if (value.ValidationState == ModelValidationState.Invalid)
                    {
                        var propertyValue = value.GetType().GetProperties()
                            .FirstOrDefault(s => s.Name == "Key")
                            ?.GetValue(value, null)?.ToString()/*.ToLower()*/;

                        if (propertyValue != null)
                        {
                            if (!result.Errors.ContainsKey(propertyValue))
                                result.Errors.Add(propertyValue, value.Errors.FirstOrDefault()?.ErrorMessage ?? ValidationMessages.INVALID_VALUE);
                        }
                    }
                }
                Log.Information($"Validation fail. {context.ActionDescriptor.DisplayName}. Request: {context.ActionArguments.ToJsonString()}. Response: {result}");
                context.Result = new BadRequestObjectResult(result);
                return Task.CompletedTask;
            }

            return base.OnActionExecutionAsync(context, next);
        }
    }
}